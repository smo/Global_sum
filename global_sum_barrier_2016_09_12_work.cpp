/* For compilation use: mpiicpc -O3 global_sum_barrier.cpp
 *
 *
 *
 *
*/

#include<mpi.h>
#include<iostream>
#include<stdlib.h>
#include<unistd.h>
#include<math.h>
#include<new>
#include<fstream>
#include<stdio.h>
#include<stdlib.h>
#include<sstream>
#include<string>
#include<cstring>
#include<cstdio>
#include<iomanip>

#define repetition     100000
#define output_rank 0


using namespace std;

int main(int argc, char* argv[])
{

double time[20];
double time_sum[20];
double time_min[20];
double time_max[20];

double loc_sum=0.0;
int ier, ntasks, rank;

ier=MPI_Init(&argc, &argv);
ier=MPI_Comm_rank(MPI_COMM_WORLD, &rank);
ier=MPI_Comm_size(MPI_COMM_WORLD, &ntasks);


//   cout<<"rank="<<rank<<"HOME="<<getenv("HOME")<<endl;  
//   cout<<"rank="<<rank<<"I_MPI_DEBUG="<<getenv("I_MPI_DEBUG")<<endl;     



typedef long long int my_idx_t;

my_idx_t loc_size,loc_b,loc_e; 


double *all_arr_mean_barrier2,*all_arr_min_barrier2, *all_arr_max_barrier2;
double *trace;

try{all_arr_mean_barrier2=new double[ntasks];}
catch(std::bad_alloc &ba)
{cerr<<" bad_alloc caught: " << ba.what() << '\n';}

try{all_arr_min_barrier2=new double[ntasks];}
catch(std::bad_alloc &ba)
{cerr<<" bad_alloc caught: " << ba.what() << '\n';}

try{all_arr_max_barrier2=new double[ntasks];}
catch(std::bad_alloc &ba)
{cerr<<" bad_alloc caught: " << ba.what() << '\n';}

try{trace=new double[repetition];}
catch (std::bad_alloc &ba)
{ cerr<<" bad_alloc caught: " << ba.what() << '\n';}


  char processor_name[MPI_MAX_PROCESSOR_NAME];
  int name_len;
  MPI_Get_processor_name(processor_name, &name_len);

   time[0]=MPI_Wtime();

   time_min[0]=10000000.0;
   time_max[0]=0.0;
   time_min[1]=10000000.0;
   time_max[1]=0.0;



   for(my_idx_t z=1;z<=repetition;z++)
     {
       // First barrier just for sinchronization
       time[1]=MPI_Wtime();
       ier=MPI_Barrier(MPI_COMM_WORLD);

       // here we measure actual execution time of the barrier
       time[2]=MPI_Wtime();
       ier=MPI_Barrier(MPI_COMM_WORLD);
       time[3]=MPI_Wtime();
       //======================================================
   
       // calculation min; max; mean;
       if(time_min[1]>(time[3]-time[2])) time_min[1]=time[3]-time[2] ;
       if(time_max[1]<(time[3]-time[2])) time_max[1]=time[3]-time[2] ;
       time_sum[1]=time_sum[1]+(time[3]-time[2]);

       // saving all data for one task
       if( (rank==output_rank) ) trace[z-1]=time[3]-time[2];

      }


   time[4]=MPI_Wtime();
   time_sum[1]/=double(repetition);


  // Gathering all data and printing the results
   MPI_Gather( &time_sum[1], 1, MPI_DOUBLE, all_arr_mean_barrier2, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
   MPI_Gather( &time_min[1], 1, MPI_DOUBLE, all_arr_min_barrier2,  1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
   MPI_Gather( &time_max[1], 1, MPI_DOUBLE, all_arr_max_barrier2,  1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  // Printing one trace
   ofstream f4;
   char file_name4[26];
   snprintf(file_name4, sizeof(file_name4), "%d_trace.txt", rank);

  if( (rank==output_rank) ) 
     {
       f4.open (file_name4);
          for(my_idx_t z=1;z<=repetition;z++) f4<<left<<setw(12)<< z<<setw(20)<<left<<trace[z-1]<<endl;
      f4.close();
      } 


   //Printing min max mean for all tasks
   if(rank==0)
   {
      char file_name3[16];
       snprintf(file_name3, sizeof(file_name3), "%d_data.txt", rank);
  
       ofstream f3;
       f3.open (file_name3);
       for(my_idx_t i=0;i<ntasks;i++) f3<<left<<setw(12)<< i<<setw(20)<<left<<all_arr_mean_barrier2[i]<<
                                        setw(20)<<all_arr_min_barrier2[i]<<
                                        setw(20)<<all_arr_max_barrier2[i]<< endl; 

       f3.close();  


    // printing of general information 
       char file_name2[16];
       snprintf(file_name2, sizeof(file_name2), "%d_info.txt", rank);

       ofstream f;
       f.open (file_name2);
       f<<"repetition="<<repetition<<endl;
       f<<"total time="<<time[4]-time[0]<<endl;
       f<<"node name="<<processor_name<<endl;
       f.close();

   }
    
   MPI_Finalize();
   return 0;
}


