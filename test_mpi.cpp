/* For compilation use: mpiicpc -O3 test_mpi.cpp
*/

#include<mpi.h>
#include<iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

int main(int argc, char* argv[])
{
int ier, ntasks, rank;

ier=MPI_Init(&argc, &argv);
ier=MPI_Comm_rank(MPI_COMM_WORLD, &rank);
ier=MPI_Comm_size(MPI_COMM_WORLD, &ntasks);

ier=MPI_Barrier(MPI_COMM_WORLD);
const char* s = getenv("HOME");
cout<<"rank="<<rank<<"HOME="<<s<<endl;
ier=MPI_Barrier(MPI_COMM_WORLD);




const char* s1 = getenv("LD_LIBRARY_PATH");
cout<<"rank="<<rank<<"LD_LIBRARY_PATH="<<s1<<endl;
ier=MPI_Barrier(MPI_COMM_WORLD);


const char* s2 = getenv("I_MPI_FABRICS");
cout<<"rank="<<rank<<"I_MPI_FABRICS="<<s2<<endl;
ier=MPI_Barrier(MPI_COMM_WORLD);



MPI_Finalize();
   return 0;
}


