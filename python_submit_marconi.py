#/usr/bin/python
import os
import sys
import time
import shutil
from subprocess import call


def generate_script_marconi(file_name,node_number,task_number):
      

          f1=open(file_name,"wb")
          f1.write("#!/bin/bash\n")
#          f1.write("#PBS -q xfuaprod\n")
          f1.write("#PBS -q xfuaprod\n")
          f1.write("#PBS -l walltime=00:55:00\n")
          f1.write("#PBS -l select="+str(node_number)+":ncpus=36:mpiprocs="+str(task_number)+":mem=118GB\n")
          f1.write("#PBS -j oe\n")
          f1.write("#PBS -A FUA10_P_HLST\n")
          f1.write("#PBS -N test_mpi\n")
          f1.write("#PBS -m ae\n")
          f1.write("#PBS -M serhiy.mochalskyy@ipp.mpg.de\n")


          f1.write("cd $PBS_O_WORKDIR\n")
          f1.write("module load intel intelmpi\n")
          f1.write("export I_MPI_DEBUG=5\n")
#          f1.write("export TMI_CONFIG=/cineca/prod/opt/compilers/intel/pe-xe-2016/binary/impi/5.1.3.210/intel64/etc/etc.conf\n")

#          f1.write("export export I_MPI_PIN_PROCESSOR_LIST=0,1\n")
#          f1.write("module load profile/advanced\n")
#          f1.write("module load profile/candidate\n")
#          f1.write("module load intel/pe-xe-2017--binary\n")
#          f1.write("module load intelmpi/2017--binary\n")

#          f1.write("module load profile/global\n")
#          f1.write("module load intel/pe-xe-2015--binary\n")
#          f1.write("module load intelmpi/5.0.3--binary\n")

          f1.write("module list\n")
          f1.write("mpirun  ./test_barrier\n")

#          f1.write("mpirun  -envlist I_MPI_DEBUG,I_MPI_FABRICS,TMI_CONFIG ./a.out\n")
#          f1.write("mpirun  -envlist I_MPI_DEBUG,I_MPI_FABRICS,LD_LIBRARY_PATH,TMI_CONFIG ./a.out\n")

          
          f1.close()
          return

FolderName = time.strftime("%Y_%m_%d_%H_%M")
print FolderName
os.mkdir(FolderName)
         
try:
    os.chdir(FolderName)
except OSError:
    print  "Error: can\'t find folder with name: "+FolderName
else:
    print "Folder " + FolderName + " was created"

binary_exe="../../test_barrier"
assert not os.path.isabs(binary_exe)
mpi_task_number=36

for x in range(0,1):
   
   node_number=2**x
   if x==0:
     node_number=64
   file_name="script_"+str(node_number)+"_nodes"


   dir_name=str(node_number)+"_nodes_global_sum";

   os.mkdir(dir_name,0755);
   os.chdir(dir_name);
   shutil.copy(binary_exe,".")


   generate_script_marconi(file_name,node_number,mpi_task_number)
   call(['qsub', file_name])

   os.chdir("../")


print "all tasks are submitted"


  

