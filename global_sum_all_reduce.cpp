#include<mpi.h>
#include<iostream>
#include<stdlib.h>
#include<unistd.h>
#include<math.h>
#include<new>
#include<fstream>
#include<stdio.h>
#include<stdlib.h>
#include<sstream>
#include<string>
#include<cstring>
#include<cstdio>
#include <iomanip>
#define n 1000000

#define repetition     60000
//#define repetition   50000000

//#define repetition 10

using namespace std;

int main(int argc, char* argv[])
{

double time[20];
double time_sum[20];
double time_min[20];
double time_max[20];

double loc_sum=0.0;
int ier, ntasks, rank;

ier=MPI_Init(&argc, &argv);
ier=MPI_Comm_rank(MPI_COMM_WORLD, &rank);
ier=MPI_Comm_size(MPI_COMM_WORLD, &ntasks);


typedef long long int my_idx_t;

my_idx_t loc_size,loc_b,loc_e; 


double *all_arr_mean_barrier2,*all_arr_min_barrier2, *all_arr_max_barrier2;
double *arr, *arr_tot;

try{all_arr_mean_barrier2=new double[ntasks];}
catch(std::bad_alloc &ba)
{cerr<<" bad_alloc caught: " << ba.what() << '\n';}

try{all_arr_min_barrier2=new double[ntasks];}
catch(std::bad_alloc &ba)
{cerr<<" bad_alloc caught: " << ba.what() << '\n';}

try{all_arr_max_barrier2=new double[ntasks];}
catch(std::bad_alloc &ba)
{cerr<<" bad_alloc caught: " << ba.what() << '\n';}




try{arr=new double[n];}
catch(std::bad_alloc &ba)
{cerr<<" bad_alloc caught: " << ba.what() << '\n';}

try{arr_tot=new double[n];}
catch(std::bad_alloc &ba)
{cerr<<" bad_alloc caught: " << ba.what() << '\n';}



  char processor_name[MPI_MAX_PROCESSOR_NAME];
  int name_len;
  MPI_Get_processor_name(processor_name, &name_len);



//--------------------------------------------
 for(my_idx_t i=0;i<loc_size;i++)  arr[i]=double(rank)*0.011;
//-----------------------------------------



   time[0]=MPI_Wtime();


   time_min[0]=10000000.0;
   time_max[0]=0.0;
   time_min[1]=10000000.0;
   time_max[1]=0.0;


   
  // char file_name4[26];
  // snprintf(file_name4, sizeof(file_name4), "%d_trace.txt", rank);

 //  ofstream f4;
 //  if( (rank%37==0) && (rank<=1000)  ) f4.open (file_name4);

   ofstream f5;
//   char file_name5[26];
//   snprintf(file_name5, sizeof(file_name5), "%d_minus.txt", rank);

//   ofstream f5;
//   f5.open (file_name5);



   for(my_idx_t z=1;z<=repetition;z++)
     {
       time[1]=MPI_Wtime();
       ier=MPI_Barrier(MPI_COMM_WORLD);
       time[2]=MPI_Wtime();
       ier= MPI_Allreduce(arr, arr_tot, n,MPI_DOUBLE, MPI_SUM,MPI_COMM_WORLD);
       time[3]=MPI_Wtime();

   
       if(time_min[1]>(time[3]-time[2])) time_min[1]=time[3]-time[2] ;
       if(time_max[1]<(time[3]-time[2])) time_max[1]=time[3]-time[2] ;
       time_sum[1]=time_sum[1]+(time[3]-time[2]);

       if((time[3]-time[2])<0.0) 
       {
        char file_name5[26];
        snprintf(file_name5, sizeof(file_name5), "%d_minus.txt", rank);
       
        f5.open (file_name5);
        f5<<z<<"    "<< time[3]-time[2]<<"    "<< time[3]<<"  "<< time[2]<<endl;
        
       }     

//       if( (rank%37==0) && (rank<=100)  )  f4<<left<<setw(12)<< z<<setw(20)<<left<<time[3]-time[2]<<endl;

      }

//  f5.close();
 
   time[4]=MPI_Wtime();
//   if( (rank%37==0) && (rank<=100)  )  f4.close();
   time_sum[1]/=double(repetition);


   MPI_Gather( &time_sum[1], 1, MPI_DOUBLE, all_arr_mean_barrier2, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
   MPI_Gather( &time_min[1], 1, MPI_DOUBLE, all_arr_min_barrier2,  1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
   MPI_Gather( &time_max[1], 1, MPI_DOUBLE, all_arr_max_barrier2,  1, MPI_DOUBLE, 0, MPI_COMM_WORLD);


   if(rank==0)
   {
       char file_name3[16];
       snprintf(file_name3, sizeof(file_name3), "%d_data.txt", rank);
  
       ofstream f3;
       f3.open (file_name3);
       for(my_idx_t i=0;i<ntasks;i++) f3<<left<<setw(12)<< i<<setw(20)<<left<<all_arr_mean_barrier2[i]<<
                                        setw(20)<<all_arr_min_barrier2[i]<<
                                        setw(20)<<all_arr_max_barrier2[i]<< endl; 


       f3.close();  

       char file_name2[16];
       snprintf(file_name2, sizeof(file_name2), "%d_info.txt", rank);

       ofstream f;
       f.open (file_name2);
       f<<"repetition="<<repetition<<endl;
       f<<"total time="<<time[4]-time[0]<<endl;
       f<<"node name="<<processor_name<<endl;
       f.close();

   }
    
//  f<<"min_time_barrier1="<<time_min[0] <<endl;
//  f<<"min_time_barrier2="<<time_min[1] <<endl;

//  f<<"max_time_barrier1="<<time_max[0] <<endl;
//  f<<"max_time_barrier2="<<time_max[1] <<endl;

//  f<<"last_time_barrier1="<<time[2]-time[1] <<endl;
//  f<<"last_time_barrier2="<<time[3]-time[2] <<endl;

//  f<<"total time="<<time[4]-time[0]<<endl;
//  f<<"node name="<<processor_name<<endl;
//  f.close();


//             arr[i]+=0.0001*(double(i)+1)-0.00012*double(j)+double(z)/124.34; 



// Second MPI_BARRIER measurements===================
//   time[2]=MPI_Wtime();
//   ier=MPI_Barrier(MPI_COMM_WORLD);
//  time[3]=MPI_Wtime();
//==================================================
      

// Second MPI_BARRIER measurements===================
//    time[4]=MPI_Wtime();
//    ier=MPI_Barrier(MPI_COMM_WORLD);
//    time[5]=MPI_Wtime();
//==================================================


//==================================================

//   time[6]=MPI_Wtime();
//   ier= MPI_Allreduce(arr, tot_arr, loc_size,MPI_DOUBLE, MPI_SUM,MPI_COMM_WORLD);
//   time[7]=MPI_Wtime();

//==================================================


//  char processor_name[MPI_MAX_PROCESSOR_NAME];
//  int name_len;
//  MPI_Get_processor_name(processor_name, &name_len);

//  double sum_total=0.0;
//  for(my_idx_t i=0;i<loc_size;i++)
//      sum_total+=tot_arr[i];


/*
if(rank % 36==0)
{
 
   char file_name[16]; 
   snprintf(file_name, sizeof(file_name), "%d.txt", rank);

   ofstream f;
   f.open (file_name);

   f <<"Number of MPI tasks= "<< ntasks<<endl;
   f <<"Number of nodes= "<<  ntasks/36.0<<endl;
   f <<"Node name is:  "<< processor_name<<endl;
  
   f <<"MPI_BARRIER1="<<  time[1]-time[0]<<endl;
   f <<"MPI_BARRIER2="<<  time[3]-time[2]<<endl;
   f <<"MPI_BARRIER3="<<  time[5]-time[4]<<endl;
   f <<"MPI_ALLReduce="<< time[7]-time[6]<<endl;
   f <<"time of calculations="<< time[2]-time[1]<<endl;
   f <<"total="<< time[7]-time[9]<<endl;
   
   f <<"sum total="<<sum_total <<endl;


   f.close();
   
}

*/

   MPI_Finalize();
   return 0;
}


