#/usr/bin/python
import os
import sys
import time
import shutil
from subprocess import call



def change_folder(FolderName,changed):
    try:
        os.chdir(FolderName)
        changed=1  
    except OSError:
        print  "Error: can\'t find folder with name: "+FolderName
        changed=0

FolderName = str(1)+"_nodes_global_sum"
f_out=open('log.txt', 'w+')


for x in range(1, 65):
       time_mean=[]
       time_max=[]
       time_min=[]
       change_folder(FolderName)  
       if changed==1:
          file_name="0_data.txt"
          if os.path.isfile(file_name):
             f1=open(file_name,"r")
             for line in f1:
               time_mean.append( float(line.split()[1]))
               time_min.append( float(line.split()[2]))
               time_max.append( float(line.split()[3]))

          if len(time_mean)>0 :
              max_time=max(time_max)
              min_time=min(time_min)
              mean_time=sum(time_mean)/len(time_mean)

              f_out.write('%-6s %-20s %-20s %-20s %-20s %-20s \n' % (str(x*36), str(mean_time), str(min_time), 
                str(max_time),str(max_time-mean_time ), str(mean_time-min_time)    ))   

       FolderName = "../"+str(x+1)+"_nodes_global_sum"
 
f_out.close()
  

