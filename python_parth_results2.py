#/usr/bin/python
import os
import sys
import time
import shutil
from subprocess import call


def change_folder(FolderName):
    try:
        os.chdir(FolderName)
    except OSError:
       print  "Error: can\'t find folder with name: "+FolderName


FolderName = str(1)+"_nodes_global_sum"
f_out=open('log.txt', 'w+')


for x in range(1, 65):
    barrier1=[]
    all_reduce=[]
   
    change_folder(FolderName)  
    for y in range(0, 4000):
        file_name=str(y)+".txt"
        if os.path.isfile(file_name):
           for line in  open(file_name,"r"):
               found = "MPI_BARRIER3" in line
               if found:
                  barrier1.append(float(line.partition("=")[2]))
               found = "MPI_ALLReduce" in line
               if found:
                  all_reduce.append(float(line.partition("=")[2]))
                           
 
    mean_barrier1=sum(barrier1)/len(barrier1)
    max_barrier1=max(barrier1)
    min_barrier1=min(barrier1)
   
    mean_all_reduce=sum(all_reduce)/len(all_reduce)
    max_all_reduce=max(all_reduce)
    min_all_reduce=min(all_reduce)


    FolderName = "../"+str(x+1)+"_nodes_global_sum"

    f_out.write('%-5s %-17s %-17s %-17s %-17s %-17s %-17s \n' % (str(x), str(mean_barrier1), 
                str(max_barrier1-mean_barrier1),str(mean_barrier1-min_barrier1),str(mean_all_reduce),  
                str(max_all_reduce-mean_all_reduce),str(mean_all_reduce-min_all_reduce)  ) )     

    if x==64:
       print all_reduce
 
#print barrier1
#print sum(barrier1)/len(barrier1), max(barrier1), min(barrier1)
#print max(all_reduce)             
        

#print line                

#file_lines.append(f1.readlines())

#print file_lines    
#                 f1.readline().partition("=")[2]
                              



  

