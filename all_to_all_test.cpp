#include<mpi.h>
#include<iostream>
#include<stdlib.h>
#include<new>
#include<fstream>
#include <cstdlib>
#include <ctime>


using namespace std;

/* globals */
int ntasks,rank,ier;
/* end globals */

void init_mpi(int  argc, char **argv);
void all_to_all(double *send_arr,double *rec_arr);
void print_arr(double *arr);
void print_results(double time1, double time2);
void max_min_mean(double &max, double &min, double &mean, double * arr);




int main(int argc, char* argv[])
{
double time[20];

clock_t begin = clock();
init_mpi(argc,argv);
clock_t end = clock();

double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;



double *send_arr,*rec_arr;

//Initialization arrays begins

try{send_arr=new double[ntasks];}
  catch(std::bad_alloc &ba)
  {cerr<<" bad_alloc caught: " << ba.what() << '\n';}

  try{rec_arr=new double[ntasks];}
  catch(std::bad_alloc &ba)
  {cerr<<" bad_alloc caught: " << ba.what() << '\n';}


  for(long long int i=0;i<ntasks;i++)
  {
   send_arr[i]=rank*ntasks+i;
   rec_arr[i]=0.0;
  }
//Initialization arrays ends


ier=MPI_Barrier(MPI_COMM_WORLD); 

time[2]=MPI_Wtime();
all_to_all(send_arr, rec_arr);
time[3]=MPI_Wtime();


print_results(elapsed_secs, time[3]-time[2]);

   MPI_Finalize();
   return 0;
}


void init_mpi(int  argc, char **argv) {
      ier=MPI_Init(&argc, &argv);
      ier=MPI_Comm_rank(MPI_COMM_WORLD, &rank);
      ier=MPI_Comm_size(MPI_COMM_WORLD, &ntasks);
}


void all_to_all(double *send_arr,double *rec_arr)
{
   ier = MPI_Alltoall(send_arr,1,MPI_DOUBLE,rec_arr,1,MPI_DOUBLE,MPI_COMM_WORLD);  
}


void print_arr(double *arr)
{
   for(long long int i=0;i<ntasks;i++)
    {
      cout<<"rank="<<rank<<"  i="<<i<<"  "<<arr[i]<<endl;
    }

}
void print_results(double time1, double time2)
{
 
  double *init_time,*alltoall_time;
  try{init_time=new double[ntasks];}
  catch(std::bad_alloc &ba)
  {cerr<<" bad_alloc caught: " << ba.what() << '\n';}

  try{alltoall_time=new double[ntasks];}
  catch(std::bad_alloc &ba)
  {cerr<<" bad_alloc caught: " << ba.what() << '\n';}

  MPI_Gather( &time2, 1, MPI_DOUBLE, alltoall_time, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Gather( &time1, 1, MPI_DOUBLE, init_time, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    
  if(rank==0) 
  {
    double max,min,mean;

    ofstream f1; 
    f1.open("output.txt");
    max_min_mean(max, min, mean, alltoall_time);
    f1<<"all_to_all: "<<max<<"  "<<min<<"  "<<mean<<endl;
    max_min_mean(max, min, mean, init_time);    
    f1<<"mpi_init: "<<max<<"  "<<min<<"  "<<mean<<endl;
    f1.close();

  }



}

void max_min_mean(double &max, double &min, double &mean, double * arr)
{
  max=0.0;
  min=1e10;
  mean=0.0; 
 
  for(long long int i=0;i<ntasks;i++)
  {
    if(arr[i]>max) max=arr[i];
    if(arr[i]<min) min=arr[i];
    mean+=arr[i];
  }
  mean/=ntasks;
}




